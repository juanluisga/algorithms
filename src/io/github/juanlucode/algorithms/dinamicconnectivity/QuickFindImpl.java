package io.github.juanlucode.algorithms.dinamicconnectivity;

public class QuickFindImpl {
	
	public static class QuickFind {
		private int[] id;
		
		QuickFind(int n) {
			id = new int[n];
			
			for (int i = 0; i < n; i++) {
				id[i] = i;
			}
		}
		
		public boolean isConnected(int p, int q) {
			return id[p] == id[q]; 
		}
		
		public void union(int p, int q) {
			int toChange = id[p];
			for (int i = 0; i < id.length; i++) {
				if (id[i] == toChange) {
					id[i] = id[q];
				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		QuickFind quickFind = new QuickFind(10);
		
		System.out.println(quickFind.isConnected(0, 9));
		
		quickFind.union(0, 9);
		
		System.out.println(quickFind.isConnected(0, 9));
		
	}

}
