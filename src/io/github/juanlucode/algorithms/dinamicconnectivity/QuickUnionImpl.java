package io.github.juanlucode.algorithms.dinamicconnectivity;

public class QuickUnionImpl {
	
	public static class QuickUnion{
		private int[] id;
		
		QuickUnion(int n){
			id = new int[n];
			for (int i = 0; i < n; i++ ) id[i] = i;
		}
		
		private int root(int node) {
			while (id[node] != node) node = id[node];
			return node;
		}
		
		public void union(int nodeChild, int nodeFather) {
			int rootChild = root(nodeChild);
			int rootFather = root(nodeFather);
			
			id[rootChild] = rootFather;
		}
		
		public boolean isConnected(int node1, int node2) {
			return root(node1) == root(node2);
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("0 1 2 3 4 5 6 7 8 9");
			sb.append("\n");
			sb.append("-------------------");
			sb.append("\n");
			for(int node = 0; node < id.length; node++) sb.append(id[node] + " ");
			sb.append("\n");
			return sb.toString();
		}
	}
	
	public static void main(String[] args) {
		
		QuickUnion quickUnion = new QuickUnion(10);
		
		System.out.println();
		quickUnion.union(4,3);
		System.out.println("union(4,3)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(3,8);
		System.out.println("union(3,8)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(6,5);
		System.out.println("union(6,5)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(9,4);
		System.out.println("union(9,4)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(2,1);
		System.out.println("union(2,1)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(8,9);
		System.out.println("(8,9)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(5,4);
		System.out.println("(5,4)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(5,0);
		System.out.println("(5,0)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(7,2);
		System.out.println("union(7,2)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(6,1);
		System.out.println("union(6,1)");
		System.out.println(quickUnion);
		
		System.out.println();
		quickUnion.union(7,3);
		System.out.println("union(7,3)");
		System.out.println(quickUnion);
		
			

	}

}
